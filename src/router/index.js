import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import day_list from '@/components/day_list'
import day_info from '@/components/day_info'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/day_list',
      name: 'day_list',
      component: day_list
    },
    {
      path: '/day_info',
      name: 'day_info',
      component: day_info
    }
  ]
})
